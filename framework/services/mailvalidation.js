import supertest from 'supertest';
import { urls } from '../config';

const Mailvalidation = function Mailvalidation() {
  this.get = async function get(access_key,email) {
    const r = await supertest(urls.mailvalidation).get(`/api/check?access_key=${access_key}&email=${email}`);
    return r;
  };
};

export { Mailvalidation };