import { test } from '@jest/globals';
import faker from 'faker';
import { apiProvider } from '../framework';


test('Test Provider mailvalidation', async () => {
    const r = await apiProvider().mailvalidation().get('e21036806efba30e6d8b9a15d2e32ca2',faker.internet.email());
    console.log(r.body);
    expect(r.status).toBe(200);
  });


describe('Test mailvalidation response fields', () => {

  test('Response has field SCORE' , async () => {
    const r = await apiProvider().mailvalidation().get('e21036806efba30e6d8b9a15d2e32ca2',faker.internet.email());
    expect(r.body).toHaveProperty('score');
  });

  test('Response has field USER' , async () => {
    const r = await apiProvider().mailvalidation().get('e21036806efba30e6d8b9a15d2e32ca2',faker.internet.email());
    expect(r.body).toHaveProperty('user');
  });

  test('No access with invalid access_key', async () => {
    const r = await apiProvider().mailvalidation().get('someinvalidkey',faker.internet.email());
    expect(r.body.error.type).toEqual("invalid_access_key");
  });
  
  test('Error 210 if User did not provide an email address', async () => {
    const r = await apiProvider().mailvalidation().get('e21036806efba30e6d8b9a15d2e32ca2','');
    expect(r.body.error.code).toBe(210);
  });  
    
  test('Field EMAIL type is string', async () => {
    const r = await apiProvider().mailvalidation().get('e21036806efba30e6d8b9a15d2e32ca2',faker.internet.email());
    expect(typeof r.body.email).toBe("string");
  });  

  test('Field SCORE must be ≤ 1', async () => {
    const r = await apiProvider().mailvalidation().get('e21036806efba30e6d8b9a15d2e32ca2',faker.internet.email());
    expect(r.body.score).toBeLessThanOrEqual(1);
  });

    });

  
test('No access without access_key', async () => {
    const r = await apiProvider().mailvalidation().get('',faker.internet.email());
    expect(r.body.error.type).toEqual("missing_access_key");
  });

